package com.codev.gotaxi.presentation.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.core.BaseFragment;
import com.codev.gotaxi.presentation.load.LoadActivity;
import com.codev.gotaxi.presentation.utils.ProgressDialogCustom;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by miguel on 14/06/17.
 */

public class RegisterPhoneFragment extends BaseFragment implements RegisterPhoneContract.View,Validator.ValidationListener{

    @BindView(R.id.tvCellphone)
    TextView tvCellphone;
    @Length(min = 9,max = 9,message = "Este campo debe tener 9 dígitos")
    @BindView(R.id.etCellphone)
    EditText etCellphone;
    Unbinder unbinder;

    private Validator validator;
    private RegisterPhoneContract.Presenter presenter;
    private ProgressDialogCustom dialogCustom;

    public static RegisterPhoneFragment newInstance() {
        return new RegisterPhoneFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_complete_register, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator = new Validator(this);
        validator.setValidationListener(this);
        dialogCustom = new ProgressDialogCustom(getContext(), "Registrando...");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void registerPhone(){
        validator.validate();
    }

    @Override
    public void registerPhoneSuccessfully() {
        nextActivity(getActivity(),null, LoadActivity.class,true);
    }

    @Override
    public void setPresenter(RegisterPhoneContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (dialogCustom != null) {
            if (active) {
                dialogCustom.show();
            } else {
                dialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onValidationSucceeded() {
        presenter.registerPhone(etCellphone.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else if (view instanceof TextView) {
                ((TextView) view).setError(message);
            } else {
                showErrorMessage(message);
            }
        }
    }
}
