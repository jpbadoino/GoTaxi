package com.codev.gotaxi.presentation.comparetaxi;

import com.codev.gotaxi.core.BasePresenter;
import com.codev.gotaxi.core.BaseView;
import com.codev.gotaxi.data.entities.CabifyEntity;
import com.codev.gotaxi.data.entities.UberEntity;
import com.google.android.gms.location.places.Place;

import java.util.ArrayList;

/**
 * Created by miguel on 31/05/17.
 */

public interface CompareTaxiContract {
    interface View extends BaseView<Presenter>{
        void getUberPrices(ArrayList<UberEntity> list);
        void getCabifyPrices(ArrayList<CabifyEntity> list);
    }
    interface Presenter extends BasePresenter{
        void compareTaxi(double startLat, double startLong, double endLat, double endLong);
    }
}
