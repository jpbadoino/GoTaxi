package com.codev.gotaxi.presentation.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.core.BaseFragment;
import com.codev.gotaxi.presentation.comparetaxi.CompareTaxiActivity;
import com.codev.gotaxi.presentation.load.LoadActivity;
import com.codev.gotaxi.presentation.register.RegisterActivity;
import com.codev.gotaxi.presentation.utils.ProgressDialogCustom;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by miguel on 24/05/17.
 */

public class LoginFragment extends BaseFragment implements LoginContract.View,Validator.ValidationListener{

    Unbinder unbinder;
    @BindView(R.id.login_facebook)
    Button loginFacebook;

    private LoginContract.Presenter presenter;
    private ProgressDialogCustom dialogCustom;
    private AccessToken accessToken;
    private Validator validator;
    private CallbackManager callbackManager;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                accessToken = loginResult.getAccessToken();
                Log.e("fb token",accessToken.toString());
                String access_token_facebook = loginResult.getAccessToken().getToken();
                if (access_token_facebook != null && !access_token_facebook.isEmpty()) {
                    presenter.loginWithFb(access_token_facebook);
                } else {
                    showErrorMessage("Algo sucedió mal al intentar loguearse");
                }

            }

            @Override
            public void onCancel() {
                showErrorMessage("El login a facebook se a cancelado, intente más tarde");
            }

            @Override
            public void onError(FacebookException error) {
                showErrorMessage("Error al intentar loguearse");
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogCustom = new ProgressDialogCustom(getContext(),"Ingresando...");
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.login_facebook})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_facebook:
                //validator.validate();
                presenter.login("kath@gmail.com","123456");


                // LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email","user_birthday"));
               // nextActivity(getActivity(),null, CompareTaxiActivity.class,true);
                break;
//            case R.id.tv_register:
//                nextActivity(getActivity(),null, RegisterActivity.class,false);
//                break;
//            case R.id.tv_forgot_password:
//                break;
//            case R.id.login:
//                //nextActivity(getActivity(),null, LoadActivity.class,true);
//                validator.validate();
//                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void loginSuccessfully() {
        nextActivity(getActivity(),null, LoadActivity.class,true);
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if(dialogCustom!=null){
            if(active){
                dialogCustom.show();
            }
            else{
                dialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity)getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity)getActivity()).showMessageError(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onValidationSucceeded() {
        //presenter.login(etEmail.getText().toString(),etPassword.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

    }
}
