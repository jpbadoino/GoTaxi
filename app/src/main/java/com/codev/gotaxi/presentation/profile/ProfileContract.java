package com.codev.gotaxi.presentation.profile;

import com.codev.gotaxi.core.BasePresenter;
import com.codev.gotaxi.core.BaseView;
import com.codev.gotaxi.data.entities.UserEntity;

/**
 * Created by miguel on 27/06/17.
 */

public interface ProfileContract {
    interface View extends BaseView<Presenter>{
        void loadProfile(UserEntity userEntity);
        void editProfile();
        void returnProfile();
    }
    interface Presenter extends BasePresenter{
        void editProfile(UserEntity userEntity);
    }
}
