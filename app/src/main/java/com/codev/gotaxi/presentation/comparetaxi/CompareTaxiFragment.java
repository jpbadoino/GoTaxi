package com.codev.gotaxi.presentation.comparetaxi;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.core.BaseFragment;
import com.codev.gotaxi.data.entities.CabifyEntity;
import com.codev.gotaxi.data.entities.UberEntity;
import com.codev.gotaxi.presentation.adapters.CabifyAdapter;
import com.codev.gotaxi.presentation.adapters.UberAdapter;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * Created by miguel on 29/05/17.
 */

public class CompareTaxiFragment extends BaseFragment implements OnMapReadyCallback, CompareTaxiContract.View, LocationListener{

    private static final String TAG = CompareTaxiActivity.class.getSimpleName();
    private static int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    @BindView(R.id.mv_compare_map)
    MapView mvCompareMap;
    Unbinder unbinder;
    @BindView(R.id.et_from_location)
    TextView etFromLocation;
    @BindView(R.id.et_to_location)
    TextView etToLocation;
    @BindView(R.id.ll_search_location)
    LinearLayout llSearchLocation;
    @BindView(R.id.ll_taxi_prices)
    LinearLayout llTaxiPrices;
    @BindView(R.id.ll_uber)
    LinearLayout llUber;
    @BindView(R.id.vi_select_uber)
    View viSelectUber;
    @BindView(R.id.vi_select_cabify)
    View viSelectCabify;
    @BindView(R.id.ll_cabify)
    LinearLayout llCabify;
    @BindView(R.id.rv_prices_uber)
    RecyclerView rvPricesUber;
    @BindView(R.id.btn_open_uber)
    Button btnOpenUber;
    @BindView(R.id.ll_price_uber)
    LinearLayout llPriceUber;
    @BindView(R.id.rv_prices_cabify)
    RecyclerView rvPricesCabify;
    @BindView(R.id.btn_open_cabify)
    Button btnOpenCabify;
    @BindView(R.id.ll_price_cabify)
    LinearLayout llPriceCabify;
    @BindView(R.id.fl_progress_bar)
    FrameLayout flProgressBar;

    private GoogleMap googleMap;
    private Place startPlace, endPlace;
    private Marker startMarker, finalMarker;
    private boolean enableIntent = true;
    private CompareView compareView = CompareView.SEARCH;
    private PlaceSearch placeSearch = PlaceSearch.START;
    private TaxiSelect taxiSelect = TaxiSelect.UBER;
    private CompareTaxiContract.Presenter presenter;

    private LinearLayoutManager cabifyManager, uberManager;
    private CabifyAdapter cabifyAdapter;
    private UberAdapter uberAdapter;
    private boolean first = true;
    private boolean first_location = true;
    private boolean end_location = false;
    double latitude, longitude;
    private AlertDialog dialogogps;
    private LocationManager mlocManager;
    LatLng center;
    Geocoder geocoder;

    public static CompareTaxiFragment newInstance() {
        return new CompareTaxiFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_compare_taxi, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mvCompareMap.onCreate(savedInstanceState);
        mvCompareMap.getMapAsync(this);

        cabifyManager = new LinearLayoutManager(getContext());
        cabifyManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPricesCabify.setLayoutManager(cabifyManager);
        cabifyAdapter = new CabifyAdapter(new ArrayList<CabifyEntity>());
        rvPricesCabify.setAdapter(cabifyAdapter);

        uberManager = new LinearLayoutManager(getContext());
        uberManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvPricesUber.setLayoutManager(uberManager);
        uberAdapter = new UberAdapter(new ArrayList<UberEntity>());
        rvPricesUber.setAdapter(uberAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mvCompareMap.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        mvCompareMap.onResume();
        enableIntent = true;
    }

    @Override
    public void onPause() {
        mvCompareMap.onPause();
        if(dialogogps!= null){
            dialogogps.dismiss();
        }
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mvCompareMap.onLowMemory();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        visibilityView();

        UiSettings uiSettings = googleMap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(false);
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getContext(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

      //getDireccion();
    }

    private void visibilityView() {
        //boolean search = compareView == CompareView.SEARCH;
        boolean price = compareView == CompareView.PRICE;
        //llSearchLocation.setVisibility(search ? View.VISIBLE : View.GONE);
        llTaxiPrices.setVisibility(price ? View.VISIBLE : View.GONE);
    }

    public void getDireccion() {
        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                center = googleMap.getCameraPosition().target;
                // btnOrigin.setEnabled(false);
                // getZonaOrigen(center.latitude, center.longitude);
                // lblDistance.setText("calculando...");
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(
                            center.latitude, center.longitude, 1);
                    if (addresses.size() > 0) {
                        Address address = addresses.get(0);

                        StringBuilder sb = new StringBuilder(128);
                        sb.append(" ");
                        sb.append(address.getAddressLine(0));
                        // if (addresses.size() > 1) {
                        //    sb.append(", ");
                        // sb.append(address.getAddressLine(1));
                        // }//
                        if (addresses.size() > 2) {
                            sb.append(", ");
                            sb.append(address.getAddressLine(2));
                        }

                        if(first_location){
                            etFromLocation.setText(sb.toString());

                            placeSearch = PlaceSearch.START;
                            compareView = CompareView.SEARCH;
                        }

                        if(end_location){
                            etToLocation.setText(sb.toString());

                            placeSearch = PlaceSearch.END;
                            compareView = CompareView.SEARCH;
                        }
                        // getZonaOrigen(center.latitude,center.longitude);

                    }
                } catch (Exception ex) {

                }
            }
        });

    }

    @OnClick({R.id.et_from_location, R.id.et_to_location, R.id.ll_uber, R.id.ll_cabify, R.id.btn_open_cabify, R.id.btn_open_uber})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_from_location:
                placeSearch = PlaceSearch.START;
                compareView = CompareView.SEARCH;
                visibilityView();
                initPlaceIntent();
                //first_location = true;
                //end_location = false;

                break;
            case R.id.et_to_location:
                placeSearch = PlaceSearch.END;
                compareView = CompareView.SEARCH;
                visibilityView();
                initPlaceIntent();
                //end_location = true;
                //first_location = false;
                break;
            case R.id.ll_cabify:
                taxiSelect = TaxiSelect.CABIFY;
                selectTaxiButton();
                break;
            case R.id.ll_uber:
                taxiSelect = TaxiSelect.UBER;
                selectTaxiButton();
                break;

            case R.id.btn_open_cabify:
                PackageManager pm2 = getContext().getPackageManager();
                try {
                    pm2.getPackageInfo("com.cabify.rider", PackageManager.GET_ACTIVITIES);
                    String uri = "cabify://cabify";
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.cabify.rider")));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.cabify.rider")));
                    }
                }
                break;

            case R.id.btn_open_uber:
                PackageManager pm = getContext().getPackageManager();
                try {
                    pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);
                    String uri = "uber://?action=setPickup";
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(uri));
                    startActivity(intent);
                } catch (PackageManager.NameNotFoundException e) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ubercab")));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.ubercab")));
                    }
                }
                break;
        }
    }

    private void locationStart() {
        mlocManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (!gpsEnabled) {
            alertafalta();
        }
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
       // mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    private void selectTaxiButton() {
        boolean uber = taxiSelect == TaxiSelect.UBER;
        boolean cabify = taxiSelect == TaxiSelect.CABIFY;
        viSelectUber.setVisibility(uber ? View.VISIBLE : View.INVISIBLE);
        viSelectCabify.setVisibility(cabify ? View.VISIBLE : View.INVISIBLE);
        llPriceUber.setVisibility(uber ? View.VISIBLE : View.GONE);
        llPriceCabify.setVisibility(cabify ? View.VISIBLE : View.GONE);
    }

    private void initPlaceIntent() {
        if(enableIntent) {
            enableIntent = false;
            try {
                AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                        .setCountry("PE")
                        .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                        .build();
                Intent intent =
                        new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                .setFilter(autocompleteFilter)
                                .build(getActivity());
                startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                // TODO: Handle the error.
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                switch (placeSearch) {
                    case START:
                        startPlace = place;
                        etFromLocation.setText(place.getAddress());
                        addMarker(place);
                        break;

                    case END:
                        endPlace = place;
                        etToLocation.setText(place.getAddress());
                        addMarker(place);
                        break;

                    default:
                        break;
                }
                reviewPlace();
            }
        }
    }

    private void addMarker(Place place){
        switch (placeSearch){
            case START:
                if(startMarker==null) {
                    startMarker = googleMap.addMarker(new MarkerOptions()
                            .position(place.getLatLng())
                            .title(place.getName().toString()));
                }
                else{
                    startMarker.setPosition(place.getLatLng());
                    startMarker.setTitle(place.getName().toString());
                }
                break;
            case END:
                if(finalMarker==null) {
                    finalMarker = googleMap.addMarker(new MarkerOptions()
                            .position(place.getLatLng())
                            .title(place.getName().toString()));
                }
                else{
                    finalMarker.setPosition(place.getLatLng());
                    finalMarker.setTitle(place.getName().toString());
                }
                break;
        }
        updateCameraView();
    }

    private void updateCameraView(){
        double lat = 0;
        double lon = 0;
        if(startMarker!=null) {
            lat = lat+startMarker.getPosition().latitude;
            lon = lon+startMarker.getPosition().longitude;
        }
        if(finalMarker!=null) {
            lat = (lat+finalMarker.getPosition().latitude)/2;
            lon = (lon+finalMarker.getPosition().longitude)/2;
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lon), 11));
    }

    private void reviewPlace() {
        boolean showView = startPlace != null && endPlace != null;
        compareView = showView ? CompareView.PRICE : CompareView.SEARCH;
      //  if(showView){presenter.compareTaxi(startPlace,endPlace);}
    }

    @Override
    public void getUberPrices(ArrayList<UberEntity> list) {
        visibilityView();
        if(uberAdapter!=null){
            uberAdapter.setItems(list);
        }
    }

    @Override
    public void getCabifyPrices(ArrayList<CabifyEntity> list) {
        if(cabifyAdapter!=null){
            cabifyAdapter.setItems(list);
        }
    }

    @Override
    public void setPresenter(CompareTaxiContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        flProgressBar.setVisibility(active?View.VISIBLE:View.GONE);
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity)getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity)getActivity()).showMessageError(message);
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        if(googleMap!= null){
            if(first){
                LatLng latLng = new LatLng( latitude, longitude);
                /*googleMap.addMarker(new MarkerOptions()
                        .position(latLng));*/
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
                first = false;
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        switch (i) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }
    }

    @Override
    public void onProviderEnabled(String s) {
        if (dialogogps != null) {
            dialogogps.dismiss();
        }
    }

    @Override
    public void onProviderDisabled(String s) {
        if(s.equals("gps")){
            alertafalta();
        }
    }

    private enum CompareView {
        SEARCH,
        PRICE
    }

    private enum PlaceSearch {
        START,
        END
    }

    private enum TaxiSelect {
        UBER,
        CABIFY
    }

    public void alertafalta() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El sistema GPS esta desactivado, para continuar presione el boton activar?");
        builder.setCancelable(false);
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent

                        (Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        });
        dialogogps = builder.create();
        dialogogps.show();
    }

    @Override
    public void onDestroy() {
        if(dialogogps!= null){
            dialogogps.dismiss();
        }
        super.onDestroy();
    }
}
