package com.codev.gotaxi.presentation.comparetaxi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codev.gotaxi.R;
import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.data.entities.UserEntity;
import com.codev.gotaxi.data.repositories.local.SessionManager;
import com.codev.gotaxi.presentation.auth.LoginActivity;
import com.codev.gotaxi.presentation.profile.ProfileActivity;
import com.codev.gotaxi.presentation.utils.ActivityUtils;
import com.codev.gotaxi.presentation.utils.CircleTransform;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 29/05/17.
 */

public class CompareTaxiActivity extends BaseActivity implements MainInterface {

    public static int UPDATE_PROFILE=2017;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.navigation)
    NavigationView navigation;
    @BindView(R.id.drawerLayout)
    DrawerLayout mDrawer;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;

    private SessionManager sessionManager;
    private Activity activity = this;
    private ActionBarDrawerToggle mDrawerToggle;

    private boolean mToolBarNavigationListenerIsRegistered = false;
    private boolean close = true;


    private MainFragment fragment;
    private FragmentInterface fragmentInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(getApplicationContext());
        setSupportActionBar(toolbar);
        setupDrawerContent(navigation);
        setupHeader(navigation);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                    //* host Activity *//*
                mDrawer,                    //* DrawerLayout object *//*
                toolbar,
                R.string.app_name,  //* "open drawer" description for accessibility *//*
                R.string.app_name  //* "close drawer" description for accessibility *//*
        );
        mDrawerToggle.syncState();
        mDrawer.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("");
        mDrawerToggle.syncState();
        findViewById(R.id.appbar).setOutlineProvider(null);

        fragment = (MainFragment) getSupportFragmentManager().findFragmentById(R.id.body);
        if (fragment == null) {
            fragment = MainFragment.newInstance();
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.body);
        }
        //new CompareTaxiPresenter(fragment);
    }


    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {


                        menuItem.setChecked(false);
                        menuItem.setCheckable(false);


                        switch (menuItem.getItemId()) {
                            case R.id.nav_connect:
                                Intent i = new Intent(getApplicationContext(),ProfileActivity.class);
                                startActivityForResult(i,1);
                                break;
                            case R.id.nav_signout:
                                sessionManager.closeSession();
                                nextActivityNewTask(activity,null,LoginActivity.class,true);
                                break;
                            default:
                                break;
                        }
                        menuItem.setChecked(false);
                        //  mDrawer.closeDrawers();
                        return true;
                    }

                });
    }

    private void setupHeader(NavigationView navigationView){
        View header = navigationView.getHeaderView(0);
        ImageView imageView = (ImageView) header.findViewById(R.id.imageView);
        TextView firstName = (TextView) header.findViewById(R.id.tv_fullnanme);
        TextView email = (TextView) header.findViewById(R.id.tv_state_gender);
        UserEntity userEntity = sessionManager.getUserEntity();
        if(userEntity.getPhoto()!=null){
            Glide.with(this).load(userEntity.getPhoto()).bitmapTransform(new CircleTransform(getApplicationContext())).into(imageView);
        }
        firstName.setText(userEntity.getFullName());
        email.setText(userEntity.getEmail());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){
            if(resultCode==UPDATE_PROFILE){
                setupHeader(navigation);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (this.mDrawer.isDrawerOpen(GravityCompat.START)) {
            this.mDrawer.closeDrawer(GravityCompat.START);
        } else {
            if(close){
                super.onBackPressed();
            }else {

                showDisplayHome(false);
            }
            //super.onBackPressed();
        }
    }



    @Override
    public void showDisplayHome(boolean enable) {
        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if(enable) {
            // Remove hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            // Show back button
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.left_arrow);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // when DrawerToggle is disabled i.e. setDrawerIndicatorEnabled(false), navigation icon
            // clicks are disabled i.e. the UP button will not work.
            // We need to add a listener, as in below, so DrawerToggle will forward
            // click events to this listener.
            if(!mToolBarNavigationListenerIsRegistered) {
                mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Doesn't have to be onBackPressed
                        onBackPressed();

                    }
                });

                mToolBarNavigationListenerIsRegistered = true;
                close = false;
            }

        } else {
            // Remove back button
            close = true;

            fragmentInterface.updateUi();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            // Show hamburger
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            // Remove the/any drawer toggle listener
            mDrawerToggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }


    }

    @Override
    public void onAttachFragment(android.support.v4.app.Fragment fragment) {
        super.onAttachFragment(fragment);
        //if(fragment.getTag().toString().equals("com.bumptech.glide.manager")){
        //    Toast.makeText(activity, "Glide Manager", Toast.LENGTH_SHORT).show();
        //}else{

        if(fragment.getTag() == null){
            fragmentInterface = (FragmentInterface) fragment;

        }

/*
        if(fragment!= null){
            if(fragment.getTag().toString().equals("com.bumptech.glide.manager")){
                Toast.makeText(activity, "Glide Manager", Toast.LENGTH_SHORT).show();
            }else{
                fragmentInterface = (FragmentInterface) fragment;

            }
        }*/
       // }
      /*  try {
            fragmentInterface = (FragmentInterface) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString() + " must implement MyInterface");
        }*/
    }

}
