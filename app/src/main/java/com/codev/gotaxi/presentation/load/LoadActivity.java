package com.codev.gotaxi.presentation.load;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.codev.gotaxi.core.BaseActivity;
import com.codev.gotaxi.data.repositories.local.SessionManager;
import com.codev.gotaxi.presentation.auth.LoginActivity;
import com.codev.gotaxi.presentation.register.RegisterPhoneActivity;

/**
 * Created by miguel on 24/05/17.
 */

public class LoadActivity extends BaseActivity {
    private SessionManager sessionManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getApplicationContext());
        initView();
    }

    private void initView(){
        if(sessionManager.isLogin()){
           /* if(sessionManager.getUserEntity().getPhone()!=null){
                nextActivityNewTask(this,null,PermisosActivity.class,true);
            }
            else{
                nextActivityNewTask(this,null,RegisterPhoneActivity.class,true);
            }*/
            nextActivityNewTask(this,null,PermisosActivity.class,true);

        }
        else{
            nextActivityNewTask(this,null,LoginActivity.class,true);
        }
    }
}
