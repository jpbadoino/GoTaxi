package com.codev.gotaxi.presentation.comparetaxi;

import com.codev.gotaxi.data.entities.CabifyBodyEntity;
import com.codev.gotaxi.data.entities.CabifyEntity;
import com.codev.gotaxi.data.entities.IconEntity;
import com.codev.gotaxi.data.entities.PricesUberEntity;
import com.codev.gotaxi.data.entities.StopsEntity;
import com.codev.gotaxi.data.entities.VehicleTypeEntity;
import com.codev.gotaxi.data.repositories.remote.ApiConstant;
import com.codev.gotaxi.data.repositories.remote.ServiceFactory;
import com.codev.gotaxi.data.repositories.remote.request.CompareTaxiRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by miguel on 31/05/17.
 */

public class CompareTaxiPresenter implements CompareTaxiContract.Presenter {

    private CompareTaxiContract.View mView;

    public CompareTaxiPresenter(CompareTaxiContract.View mView) {
        this.mView = mView;
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {

    }

    @Override
    public void compareTaxi(final double startLat, final double startLong, final double endLat, final double endLong) {
        mView.setLoadingIndicator(true);
        CompareTaxiRequest request = ServiceFactory.createUberService(CompareTaxiRequest.class);
         Call<PricesUberEntity> call = request.getUberPrices("Token "+ ApiConstant.TOKEN_UBER,startLat, startLong,endLat,endLong);
        call.enqueue(new Callback<PricesUberEntity>() {
            @Override
            public void onResponse(Call<PricesUberEntity> call, Response<PricesUberEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if(response.isSuccessful()){
                    getCabifyPrices(startLat,startLong, endLat, endLong);
                    mView.getUberPrices(response.body().getPrices());
                }
                else {
                    mView.setLoadingIndicator(false);
                    mView.showErrorMessage("Hubo un error, por favor intente más tarde");
                }
            }

            @Override
            public void onFailure(Call<PricesUberEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }

    private void getCabifyPrices(final double startLat, final double startLong, final double endLat, final double endLong){
        CompareTaxiRequest request = ServiceFactory.createCabifyService(CompareTaxiRequest.class);
        Call<ArrayList<CabifyEntity>> call = request.getCabifyPrices("Bearer "+ApiConstant.TOKEN_CABIFY,"es",
                formCabifyBody(startLat,startLong,endLat, endLong));
        call.enqueue(new Callback<ArrayList<CabifyEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<CabifyEntity>> call, Response<ArrayList<CabifyEntity>> response) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                if(response.isSuccessful()){
                    mView.getCabifyPrices(response.body());
                }
                else{
                    ArrayList<CabifyEntity> list =  new ArrayList<>();
                    CabifyEntity cabifyEntity = new CabifyEntity();
                    VehicleTypeEntity vehicleTypeEntity = new VehicleTypeEntity();
                    vehicleTypeEntity.setName("Tenemos un error,");
                    IconEntity iconEntity = new IconEntity();
                    iconEntity.setRegular("SIN FOTO");
                    vehicleTypeEntity.setIcons(iconEntity);
                    cabifyEntity.setVehicle_type(vehicleTypeEntity);
                    cabifyEntity.setFormatted_price("Intente de nuevo por favor");
                    list.add(cabifyEntity);
                    mView.getCabifyPrices(list);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CabifyEntity>> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("No se pudo conectar al servidor, por favor intente más tarde");
            }
        });
    }

    private CabifyBodyEntity formCabifyBody(double startLat, double startLong, double endLat, double endLong){
        ArrayList<StopsEntity> stopsEntity = new ArrayList<>();
        stopsEntity.add(new StopsEntity(new double[]{startLat,startLong}));
        stopsEntity.add(new StopsEntity(new double[]{endLat,endLong}));
        return new CabifyBodyEntity(stopsEntity);
    }
}
