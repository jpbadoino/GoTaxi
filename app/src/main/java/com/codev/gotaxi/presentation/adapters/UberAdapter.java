package com.codev.gotaxi.presentation.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codev.gotaxi.R;
import com.codev.gotaxi.data.entities.UberEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by miguel on 19/06/17.
 */

public class UberAdapter extends RecyclerView.Adapter<UberAdapter.ViewHolder> {

    private ArrayList<UberEntity> list;

    public UberAdapter(ArrayList<UberEntity> list) {
        this.list = list;
    }

    public void setItems(ArrayList<UberEntity> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_taxi, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UberEntity uberEntity = list.get(position);
        holder.tvTaxiType.setText(uberEntity.getDisplay_name());
        double number = (uberEntity.getHigh_estimate()+uberEntity.getLow_estimate())/2;
        holder.tvTaxiPrice.setText(uberEntity.getCurrency_code()+" "+number);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_taxi_type)
        TextView tvTaxiType;
        @BindView(R.id.tv_taxi_price)
        TextView tvTaxiPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
