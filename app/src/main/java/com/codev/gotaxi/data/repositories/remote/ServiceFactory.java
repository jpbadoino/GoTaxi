package com.codev.gotaxi.data.repositories.remote;



import com.codev.gotaxi.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Service Factory for Retrofit
 */
public class ServiceFactory {

    private static final String API_BASE_URL_UBER = BuildConfig.BASE_UBER;
    private static final String API_BASE_URL_CABIFY = BuildConfig.BASE_CABIFY;
    private static final String API_BASE_URL = BuildConfig.BASE_URL;

// set your desired log level

    private static OkHttpClient httpClient = new OkHttpClient();
    private static Retrofit.Builder builderUber =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL_UBER)
                    .addConverterFactory(GsonConverterFactory.create());
    private static Retrofit.Builder builderCabify =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL_CABIFY)
                    .addConverterFactory(GsonConverterFactory.create());
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createUberService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit retrofit = builderUber.client(httpClient).client(client).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createCabifyService(Class<S> serviceClass){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit retrofit = builderCabify.client(httpClient).client(client).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit retrofit = builder.client(httpClient).client(client).build();
        return retrofit.create(serviceClass);
    }
}
