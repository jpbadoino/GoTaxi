package com.codev.gotaxi.data.repositories.local;

import android.content.Context;
import android.content.SharedPreferences;


import com.codev.gotaxi.data.entities.AccessTokenEntity;
import com.codev.gotaxi.data.entities.UserEntity;
import com.google.gson.Gson;

/**
 * Created by junior on 13/10/16.
 */
public class SessionManager {


    private static final String PREFERENCE_NAME = "SymbiosisClient";
    private static int PRIVATE_MODE = 0;

    /**
     USUARIO DATA SESSION - JSON
     */
    private static final String USER_TOKEN = "user_code";
    private static final String USER_JSON = "user_json";
    private static final String IS_LOGIN = "user_login";



    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences(PREFERENCE_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public boolean isLogin()  {
        return preferences.getBoolean(IS_LOGIN, false);
    }


    public void openSession(AccessTokenEntity token,UserEntity userEntity) {
        editor.putBoolean(IS_LOGIN, true);
        String accessToken = new Gson().toJson(token);
        editor.putString(USER_TOKEN, accessToken);
        String user = new Gson().toJson(userEntity);
        editor.putString(USER_JSON,user);
        editor.commit();
    }

    public void closeSession() {
        editor.putBoolean(IS_LOGIN, false);
        editor.putString(USER_TOKEN, null);
        editor.putString(USER_JSON, null);
        editor.commit();
    }


    public AccessTokenEntity getUserToken() {
        AccessTokenEntity accessTokenEntity = null;
        if (isLogin()) {
             accessTokenEntity= new Gson().fromJson(preferences.getString(USER_TOKEN,"")
                    ,AccessTokenEntity.class);
        }
        return accessTokenEntity;
    }

    public void setUser(UserEntity userEntity) {
        if (userEntity != null) {
            Gson gson = new Gson();
            String user = gson.toJson(userEntity);
            editor.putString(USER_JSON, user);
        }
        editor.commit();
    }

    public UserEntity getUserEntity() {
        String userData = preferences.getString(USER_JSON, null);
        return new Gson().fromJson(userData, UserEntity.class);
    }

}
