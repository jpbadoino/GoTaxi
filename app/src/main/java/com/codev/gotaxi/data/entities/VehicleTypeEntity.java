package com.codev.gotaxi.data.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by miguel on 31/05/17.
 */

public class VehicleTypeEntity implements Serializable {
    @SerializedName("_id")
    private String id;
    private String name;
    private String short_name;
    private String description;
    private IconEntity icons;
    private boolean reserved_only;
    private boolean asap_only;
    private String icon;
    private EtaEntity eta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IconEntity getIcons() {
        return icons;
    }

    public void setIcons(IconEntity icons) {
        this.icons = icons;
    }

    public boolean isReserved_only() {
        return reserved_only;
    }

    public void setReserved_only(boolean reserved_only) {
        this.reserved_only = reserved_only;
    }

    public boolean isAsap_only() {
        return asap_only;
    }

    public void setAsap_only(boolean asap_only) {
        this.asap_only = asap_only;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
