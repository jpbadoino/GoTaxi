package com.codev.gotaxi.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by miguel on 19/06/17.
 */

public class CabifyBodyEntity implements Serializable {
    private ArrayList<StopsEntity> stops;

    public CabifyBodyEntity(ArrayList<StopsEntity> stops) {
        this.stops = stops;
    }

    public ArrayList<StopsEntity> getStops() {
        return stops;
    }

    public void setStops(ArrayList<StopsEntity> stops) {
        this.stops = stops;
    }
}
