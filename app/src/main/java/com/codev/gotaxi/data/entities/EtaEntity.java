package com.codev.gotaxi.data.entities;

import java.io.Serializable;

/**
 * Created by miguel on 31/05/17.
 */

public class EtaEntity implements Serializable {
    private double min;
    private double max;
    private String formatted;

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public String getFormatted() {
        return formatted;
    }

    public void setFormatted(String formatted) {
        this.formatted = formatted;
    }
}
